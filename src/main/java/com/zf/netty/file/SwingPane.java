package com.zf.netty.file;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.net.URI;
import java.util.Hashtable;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class SwingPane extends JFrame {
	private static final Logger LOGGER = Logger.getLogger(SwingPane.class);
	private static final long serialVersionUID = 1L;
	private JLabel label;
	private Icon icon;
	private String qrContent;

	public SwingPane(String qrContent, WindowListener listener) {
		this.qrContent = qrContent;
		addWindowListener(listener);
	}

	public void go() throws WriterException {
		// 居中
		setLocationRelativeTo(null); 
		Container panel = this.getContentPane();  
		// 布局方式
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));  
//		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		// 标题
		setTitle(Main.appTitle);
		// 大小
		setSize(300, 400);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		// 二维码
		label = new JLabel();
		panel.add(label);
		Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		BitMatrix matrix = new MultiFormatWriter().encode(qrContent, BarcodeFormat.QR_CODE, 300, 300, hints);
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, matrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
			}
		}
		icon = new ImageIcon(image);
		label.setIcon(icon);
		label.setHorizontalAlignment(0);
		// 文件路径显示
		add(new JLabel("当前文件："));
		JLabel filelabel = new JLabel(Main.fileName);
		filelabel.setToolTipText(Main.fileName);
		filelabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));//设置鼠标样式
		add(filelabel);
		// 超链接
		JLabel linklabel = new JLabel(Main.appStartDesc);
		linklabel.setForeground(Color.BLUE);//设置链接颜色  
		linklabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));//设置鼠标样式 
		linklabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				Desktop desktop = Desktop.getDesktop();
				try {
					desktop.browse(new URI(Main.appUrl));
				} catch (Exception e1) {
					LOGGER.error(e1);
				}
			}
		});
		panel.add(linklabel);
		setVisible(true);
	}
}
